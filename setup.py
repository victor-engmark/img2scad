#!/usr/bin/env python
"""
Package setup.
"""
from os.path import dirname, join

from setuptools import find_packages, setup

from img2scad.img2scad import __author__, __doc__, __email__

with open(join(dirname(__file__), "version.txt"), encoding="utf-8") as file_handle:
    version = file_handle.read().rstrip("\n")

setup(
    name="img2scad",
    version=version,
    description="Image to OpenSCAD converter",
    long_description=__doc__,
    url="https://gitlab.com/engmark/img2scad",
    keywords="image images height heightmap heightmaps elevation convert converter OpenSCAD SCAD",
    packages=find_packages(exclude=["tests"]),
    install_requires=["Pillow==10.3.0"],
    entry_points={"console_scripts": ["img2scad=img2scad.img2scad:main"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Artistic Software",
        "Topic :: Multimedia :: Graphics :: 3D Modeling",
        "Topic :: Multimedia :: Graphics :: Graphics Conversion",
    ],
    test_suite="tests.tests",
    author=__author__,
    author_email=__email__,
    maintainer=__author__,
    maintainer_email=__email__,
    download_url="https://gitlab.com/engmark/img2scad",
    platforms=["POSIX", "Windows"],
    license="GPL v3 or newer",
)
