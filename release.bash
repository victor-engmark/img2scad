#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

script_directory="$(dirname "${BASH_SOURCE[0]}")"

# shellcheck source=common.bash
. "${script_directory}/common.bash"

twine upload "${distribution_directory}/"*

tag="v$(cat "${script_directory}/version.txt")"
git tag --annotate --message='PyPI release' "$tag"
git push origin "$tag"
