let
  pkgs = import ./nixpkgs.nix;
  python = (pkgs.callPackage ./python.nix {}).withPackages (
    ps: [
      ps.black
      ps.check-manifest
      ps.coverage
      ps.isort
      ps.mypy
      ps.pylint
      ps.setuptools
      ps.types-setuptools
      ps.twine
    ]
  );
  app = python.pkgs.toPythonApplication (pkgs.callPackage ./derivation.nix {});
in
  pkgs.mkShell {
    packages = [
      pkgs.alejandra
      app
      pkgs.cacert
      pkgs.check-jsonschema
      pkgs.gitFull
      pkgs.gitlint
      pkgs.nix
      pkgs.nodePackages.prettier
      pkgs.pre-commit
      pkgs.shellcheck
      pkgs.yq-go
      python
    ];
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  }
