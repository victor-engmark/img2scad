#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

script_directory="$(dirname "${BASH_SOURCE[0]}")"

# shellcheck source=common.bash
. "${script_directory}/common.bash"

# Work around result-dist not being editable
mkdir "$distribution_directory"
nix-build --attr dist
install --mode=0600 "${script_directory}/result-dist/"* "$distribution_directory"
