# img2scad

Convert black and white images to OpenSCAD structures.

Installation / upgrade: pip install --upgrade img2scad

## Release

1. Bump the version in [version.txt](version.txt)
2. Commit & push everything
3. [Create a new merge request](https://gitlab.com/engmark/img2scad/-/merge_requests/new)
4. Wait for the MR to be merged
5. Update to latest `master`
6. `nix-shell --pure --run './clean.bash && ./distribute.bash && ./release.bash'`

Requirements:

-  Nix
