{pkgs}: let
  pname = "img2scad";
  version = pkgs.lib.removeSuffix "\n" (builtins.readFile ./version.txt);
  python = pkgs.callPackage ./python.nix {};
in
  python.pkgs.buildPythonPackage {
    inherit pname version;

    src = builtins.path {
      path = ./.;
      name = pname;
    };

    propagatedBuildInputs = [
      python.pkgs.pillow
      python.pkgs.types-pillow
    ];

    pythonImportsCheck = [
      pname
    ];

    meta = {
      homepage = "https://gitlab.com/engmark/${pname}";
      downloadPage = "https://pypi.org/project/${pname}/";
      description = "Convert black and white images to OpenSCAD structures";
      longDescription = builtins.readFile ./README.md;
      mainProgram = pname;
      maintainers = [
        pkgs.lib.maintainers.l0b0
      ];
      license = pkgs.lib.licenses.gpl3Plus;
    };
  }
